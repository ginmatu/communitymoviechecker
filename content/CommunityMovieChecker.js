var CommunityMovieChecker = {
	info: {},

	check: function() {
		clearTimeout(CommunityMovieChecker.timer);
		var comm_id = document.getElementById("comm_id").getAttribute('value');
		if(comm_id.match(/^co\d+$/)) {
			this.comm_id = comm_id;
			this.page = 1;
			this._check();
			return;
		}
		this.alert("co???(?は数字)と入力してください");
	},

	_check: function() {
		var url = "http://com.nicovideo.jp/video/" + this.comm_id + "?page=" + this.page + "&sort=c&order=a";
		document.getElementById('page').setAttribute('src', url );
	},

	alert: function(text) {
		let prompts = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
		              .getService(Components.interfaces.nsIPromptService);
		prompts.alert(window, "Community Movie Checker", text);
	},

	onLoad: function(e) {
		clearTimeout(CommunityMovieChecker.timer);
		var doc = e.target;
		var location = doc.location;
		var errorFlag = false;
		if(!location.href.match(/^http:\/\/com\.nicovideo\.jp\/video\/co\d+\?/)) {
			errorFlag = true;
		}
		if(!errorFlag) {
			for(var i=0,t;t=location.href.split("?")[1].split("&")[i];i++) {
				var k = t.split("=");
				switch(k[0]) {
				case "page":
					CommunityMovieChecker.page = parseInt(k[1], 10);
					break;
				case "sort":
					if(k[1]!="c") errorFlag = true;
					break;
				case "order":
					if(k[1]!="a") errorFlag = true;
					break;
				}
			}
		}
		if(errorFlag) {
			CommunityMovieChecker.alert("ダメよ～、ダメダメ♡");
			document.getElementById('page').goBack();
			return;
		}
		var c = Number.NaN;
		var tmp = doc.getElementById('site-body').innerHTML;
		if(tmp.match(/<h1>短時間での連続アクセスはご遠慮ください<\/h1>/)) {
			CommunityMovieChecker.timer = setTimeout(function(){
				CommunityMovieChecker.reload();
			}, 60000);
			return;
		}
		for(var i=0,d;d=doc.getElementsByClassName('body')[i];i++){
			if(d.innerHTML.match(/コミュニティ動画一覧\s+\((\d+)\)/)) {
				c = parseInt(RegExp.$1,10);
				break;
			}
		}
		if(isNaN(c)) {
			CommunityMovieChecker.alert("Error");
			return;
		}
		CommunityMovieChecker.maxpage = Math.ceil(c/20);
		var status = CommunityMovieChecker.page + '/' + CommunityMovieChecker.maxpage;
		document.getElementById('panel').setAttribute('label',status);
		document.getElementById('progress_meter').setAttribute('max', CommunityMovieChecker.maxpage);
		document.getElementById('progress_meter').setAttribute('value', CommunityMovieChecker.page);
		var tids = [];
		CommunityMovieChecker.info[CommunityMovieChecker.page] = {};
		for(var i = 0, tbl; tbl = doc.getElementsByTagName('table')[i]; i++) {
			if(tbl.getAttribute('width') != 713)
				continue;
			for(var j = 0, tr; tr = tbl.getElementsByTagName('tr')[j]; j++) {
				if(tr.getAttribute('valign') !='top')
					continue;
				var a = tr.getElementsByTagName('td')[0]
				          .getElementsByTagName('div')[0]
				          .getElementsByTagName('a')[0];
				var href = a.getAttribute('href');
				if(href.match(/^http:\/\/www\.nicovideo\.jp\/watch\/(\d{10})$/)) {
					var tid = RegExp.$1;
					var info = {};
					info.index = tids.length;
					info.title = a.getElementsByTagName('img')[0].getAttribute('title');
					CommunityMovieChecker.info[CommunityMovieChecker.page][tid] = info;
					tids.push(tid);
				}
			}
		}
		if(tids.length > 0)
			CommunityMovieChecker.getVideoArray(tids);
	},

	reload: function() {
		clearTimeout(CommunityMovieChecker.timer);
		document.getElementById('page').reload();
	},

	getVideoArray: function(tids){
		var url = 'http://api.ce.nicovideo.jp/nicoapi/v1/video.array?v=' + tids.join(',');
		var req = new XMLHttpRequest();
		if( !req )
			return;

		req.open('GET',url);
		req.timeout = 30*1000;
		req.onreadystatechange = function(){
			if( req.readyState == 4 && req.status == 200 ){
				var xml = req.responseXML;
				for(var i=0,vi;vi=xml.getElementsByTagName('video_info')[i];i++) {
					var tid = vi.getElementsByTagName('thread')[0].getElementsByTagName('id')[0].textContent;
					var vid = vi.getElementsByTagName('video')[0].getElementsByTagName('id')[0].textContent;
					var title = vi.getElementsByTagName('video')[0].getElementsByTagName('title')[0].textContent;
					var deleted = vi.getElementsByTagName('video')[0].getElementsByTagName('deleted')[0].textContent;
					if(deleted != '0')
						vid = '---';
					CommunityMovieChecker.info[CommunityMovieChecker.page][tid].vid = vid;
					CommunityMovieChecker.info[CommunityMovieChecker.page][tid].title = title;
					CommunityMovieChecker.info[CommunityMovieChecker.page][tid].deleted = deleted;
				}

				CommunityMovieChecker.setLog();

				if(CommunityMovieChecker.page < CommunityMovieChecker.maxpage) {
					CommunityMovieChecker.page++;
					CommunityMovieChecker.timer = setTimeout(function(){
						CommunityMovieChecker._check();
					},0);
				}else{
					CommunityMovieChecker.alert('Finish!');
				}
			}
		};
		req.send(null);
	},

	setLog: function() {
		var log = '';
		for(var i = 1,info; info = CommunityMovieChecker.info[i]; i++) {
			log += i + 'ページ\n';
			for(var tid in info) {
				log += tid + '\t' + (info[tid].vid || '---') + '\t' + info[tid].title + '\n';
			}
		}
		document.getElementById("log").value = log;
	},

	init: function() {
		document.getElementById('page').addEventListener('DOMContentLoaded', this.onLoad, false );
	},

	destroy: function(){
		clearTimeout(CommunityMovieChecker.timer);
	},
};

window.addEventListener("load", function(e){ CommunityMovieChecker.init(); }, false);
window.addEventListener("unload", function(e){ CommunityMovieChecker.destroy(); }, false);
